/**
 * Created by zachary on 6/5/21.
 */

@IsTest
private class classPassTest {
    @IsTest
    static void testBulkify() {
        List<Account> accounts = new List<Account>();
        List<Contact> contacts = new List<Contact>();
        for(Integer i = 0; i < 200; i++) {
            Account account = new Account();
            account.Name = 'test ' + i;
            accounts.add(account);
        }
        insert accounts;
        Map<Id, Account> accountsByIds = new Map<Id, Account>([SELECT Id, Name FROM Account]);
        for(Account account : accounts) {
            Contact contact = new Contact();
            contact.FirstName = 'test';
            contact.LastName = account.Name;
            contact.AccountId = account.Id;
            contacts.add(contact);
        }
        insert contacts;
        Map<Id, Contact> contactsByIds = new Map<Id, Contact>([SELECT Id, Name, AccountId FROM Contact]);
        Test.startTest();
        String jsonString = classPass.bulkify();
        List<JSONParse> jsons = new JSONParse(jsonString).asList();
        System.assertEquals(200, jsons.size());
        for(JSONParse jsonObject : jsons) {
            String contactId = jsonObject.get('Contact Id').getStringValue();
            String contactName = jsonObject.get('Contact Name').getStringValue();
            String accountId = jsonObject.get('Account').get('Account Id').getStringValue();
            String accountName = jsonObject.get('Account').get('Account Name').getStringValue();
            System.assertEquals(contactsByIds.get(contactId).Name, contactName);
            System.assertEquals(contactsByIds.get(contactId).AccountId, accountId);
            System.assertEquals(accountsByIds.get(accountId).Name, accountName);
        }
        Test.stopTest();
    }
}
