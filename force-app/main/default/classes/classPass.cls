/**
 * Created by zachary on 6/5/21.
 */

public with sharing class classPass {
    public static String bulkify() {
        List<Contact> contacts = [SELECT Id, AccountId, Name FROM Contact LIMIT 200];
        Map<Id, Contact> contactsByAccountId = new Map<Id, Contact>();
        for(Contact contact : contacts) {
            contactsByAccountId.put(contact.AccountId, contact);
        }
        Map<Id, Account> accountsById = new Map<Id, Account>([SELECT Id, Name FROM Account WHERE Id IN :contactsByAccountId.keySet()]);
        JSONGenerator generator = JSON.createGenerator(true);
        generator.writeStartArray();
        for(Id accountId : contactsByAccountId.keySet()) {
            Account account = accountsById.get(accountId);
            Contact contact = contactsByAccountId.get(accountId);
            generator.writeStartObject();
            generator.writeStringField('Contact Id', contact.Id);
            generator.writeStringField('Contact Name', contact.Name);
            generator.writeFieldName('Account');
            generator.writeStartObject();
            generator.writeStringField('Account Id', accountId);
            generator.writeStringField('Account Name', account.Name);
            generator.writeEndObject();
            generator.writeEndObject();
        }
        generator.writeEndArray();
        String jsonString = generator.getAsString();
        return jsonString;
    }
}
